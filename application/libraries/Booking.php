<?php
defined('BASEPATH') OR exit('No direct script access allowed');

ini_set('session.hash_bits_per_character', 5);
include('CheckfrontAPI.php');

class Checkfront extends CheckfrontAPI {

	public $tmp_file = '.checkfront_oauth';

	public function __construct($data) {
		parent::__construct($data,session_id());
	}

	/* DUMMY Data store.  This sample stores oauth tokens in a text file...
	 * This is NOT reccomened in production.  Ideally, this would be in an encryped 
	 * database or other secure source.  Never expose the client_secret or access / refresh
	 * tokens.
	 *
	 * store() is called from the parent::CheckfrontAPI when fetching or setting access tokens.  
	 *
	 * When an array is passed, the store should save and return the access tokens.
	 * When no array is passed, it should fetch and return the stored access tokens.
	 *
	 * param array $data ( access_token, refresh_token, expire_token )
	 * return array
	 */
	final protected function store($data=array()) {
		$tmp_file = sys_get_temp_dir() . DIRECTORY_SEPARATOR. $this->tmp_file;
		if(count($data)  ) {
			file_put_contents($tmp_file,json_encode($data,true));
		} elseif(is_file($tmp_file)) {
			$data = json_decode(trim(file_get_contents($tmp_file)),true);
		}
		return $data;
	}

	public function session($session_id,$data=array()) {
		session_id($session_id);
		if(!empty($data)) $_SESSION = $data;
	}
}

class Booking {

	public $cart = array();
	public $session = array();

	function __construct() {
		// apply a session_id to the request if one is specified
		if (!empty($_GET['cart_id'])) session_id($_GET['cart_id']);
		//session_start();
		// create api connection to Checkfront
		// you can generate a token pair under Manage / Developer in your account
		// rajesh surapuraju test 
		$this->Checkfront = new Checkfront(
			array(
				'host' => 'longtermstay.checkfront.com',
				'auth_type' => 'token',
				'api_key' => '044fdf4b9ee42f122cc9e6019e79d70c4155322e',
				'api_secret' => 'ccbd25f429ad83c406e8b5f37ef8ab4916193511e8b5931822598520da8b98d6',
				'account_id' => 'off'
			)
		);
		// init shopping cart
		$this->cart();
	}

	// fetch items from inventory based on date
	public function query_category() {
/*				$response = Array(
						"9"=> Array(
							"category_id"=> 9,
							"name"=> "Milipitas Private",
							"description"=> "",
							"pos"=> 26,
							"qty"=> 0,
							"unit"=> "N"
						),
						"10"=> Array(
							"category_id"=> 10,
							"name"=> "Milipitas Shared",
							"description"=> "",
							"pos"=> 5,
							"qty"=> 0,
							"unit"=> "N"
						)
				);
		return $response;*/
		$response = $this->Checkfront->get('category');
		return $response['category'];
	}

	// fetch items from inventory based on date
	public function query_inventory($data) {
/*				$response = Array( 
					"1" => Array(
						"item_id"=> 1,
						"name"=> "MP01 Master",
						"summary"=> "a spacious 1 bed room",
						"rate"=> Array(
							"slip"=> "sfsdfkj32r0",
							"available"=> 1,
							"summary"=> Array(
								"title" => "Available",
                            	"details" => "Qty: 30 Days @ $80.00",
                            	"price" => Array
								(
                                    "total" => "$2,400.00",
                                    "title" => "$80.00",
                                    "unit" => "per day",
                                    "param" => Array
                                        (
                                            "q" => "$80.00"
                                        )
                                ),
								"date"=> "Fri Apr 15 2016 - Sat May 14 2016"
							)
						),
						"meta"=> Array(
							"location"=> Array(
								"str"=> "1900 Lee Way, Milpitas, CA 95035, USA",
								"lat"=> "37.4060364",
								"lng"=> "-121.899664",
								"title"=> "MP01 Master",
								"description"=> "Milpitas Paying guest accommodation",
								"link_txt"=> "Open in Google Maps",
								"link"=> "https://www.google.com/maps/place/1900+Lee+Way,+Milpitas,+CA+95035,+USA",
								"zoom"=> 13
							)
						),
						"image"=> Array(
							"1"=> Array(
								"title"=> "", 
								"src"=> "31-1--4515",
								"path"=> "http://[::1]/ltstay/assets/bootstrap/img/amenities6.jpg"
							),
							"2"=> Array(
								"title"=> "", 
								"src"=> "31-2--4523",
								"path"=> "http://[::1]/ltstay/assets/bootstrap/img/amenities1.jpg"
							),
							"3"=> Array(
								"title"=> "", 
								"src"=> "31-2--4523",
								"path"=> "http://[::1]/ltstay/assets/bootstrap/img/amenities2.jpg"
							),
							"4"=> Array(
								"title"=> "", 
								"src"=> "31-2--4523",
								"path"=> "http://[::1]/ltstay/assets/bootstrap/img/amenities3.jpg"
							),
							"5"=> Array(
								"title"=> "", 
								"src"=> "31-2--4523",
								"path"=> "http://[::1]/ltstay/assets/bootstrap/img/amenities4.jpg"
							),
							"6"=> Array(
								"title"=> "", 
								"src"=> "31-2--4523",
								"path"=> "http://[::1]/ltstay/assets/bootstrap/img/amenities5.jpg"
							)
						),
						"category_id"=> 9
					)
				);
		return $response;*/
		$response = $this->Checkfront->get('item',array('start_date'=>$data['start_date'],'end_date'=>$data['end_date'],'keyword'=>$data['keyword']));
		return $response['items'];
	}

	// add slips to the booking session
	public function set($slips=array()) {
		$response = $this->Checkfront->post('booking/session',array('slip'=>$slips));
		$this->Checkfront->set_session($response['booking']['session']['id'], $response['booking']['session']);
		$this->cart();
	}

	// get the booking form fields required to make a booking
	public function form() {
		$response = $this->Checkfront->get('booking/form');
		return $response['booking_form_ui'];
	}

	// get cart session
	public function cart() {
		if(!empty($_SESSION)) {
			$response = $this->Checkfront->get('booking/session');
			if(!empty($_SESSION['inputs']) && !empty($response['booking']['session'])) {
				$response['booking']['session']['inputs'] = $_SESSION['inputs'];
			}
			if(!empty($response['booking']['session']['item'])) {
				foreach($response['booking']['session']['item']  as $line_id => $data) {
					// store for later
					$this->cart[$line_id] = $data;
				}
			}
			$this->Checkfront->set_session($response['booking']['session']['id'], $response['booking']['session']);
		}
	}

	// create a booking using the session and the posted form fields
	public function create($form) {
		$form['session_id'] = session_id();
		if($response = $this->Checkfront->post('booking/create',array('form'=>$form))) {
			return $response;
		}
	}

	// clear the current remote session
	public function clear() {
		$response = $this->Checkfront->get('booking/session/clear');
		session_destroy();
	}	
}
