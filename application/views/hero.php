<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class='hero'>
	<h1 class='hero-text'>Never Miss Your Home.</h1>
	<img src='<?php echo asset_url();?>img/ltstay_hero<?php echo rand(1,6);?>.jpg' alt='long term stay' class='hero-img'></img>
</div>
