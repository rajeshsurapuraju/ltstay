<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="book page row">
	<div class='heading-wrapper row'>
		<h2 class='heading'>Booking Details</h2>
	</div>
	<div class='content-wrapper row'>
		<div class='book-right-col col-xs-12 col-sm-3 col-sm-push-9 col-md-3 col-md-push-9'>
			<div class='book-summary'>
				<h4>Summary</h4>
				<div class='row'>
					<div class='col-xs-7 col-sm-7 col-md-7'>
						Rooms: 
					</div>
					<div class='col-xs-5 col-sm-5 col-md-5'>
						<?php echo $session['qty']; ?>
					</div>
				</div>
				<div class='row'>
					<div class='col-xs-7 col-sm-7 col-md-7'>
						Sub Total:
					</div>
					<div class='col-xs-5 col-sm-5 col-md-5'>
						$<?php echo $session['sub_total']; ?>
					</div>
				</div>
				<div class='row'>
					<div class='col-xs-7 col-sm-7 col-md-7'>
						Tax Total:
					</div>
					<div class='col-xs-5 col-sm-5 col-md-5'>
						$<?php echo $session['tax_total']; ?>
					</div>
				</div>
				<div class='row'>
					<div class='col-xs-7 col-sm-7 col-md-7'>
						Discount:
					</div>
					<div class='col-xs-5 col-sm-5 col-md-5'>
						$<?php echo $session['discount']; ?>
					</div>
				</div>
				<div class='row'>
					<div class='col-xs-7 col-sm-7 col-md-7'>
						<b>Total(USD):</b>
					</div>
					<div class='col-xs-5 col-sm-5 col-md-5'>
						<b>$<?php echo $session['total']; ?></b>
					</div>
				</div>
				<div class='row dotted-seperator'></div>
				<div class='row'>
					<div class='col-xs-7 col-sm-7 col-md-7'>
						Deposit Total:
					</div>
					<div class='col-xs-5 col-sm-5 col-md-5'>
						$<?php echo $session['deposit']['total']; ?>
					</div>
				</div>
				<div class='row book-final'>
					<div class='col-xs-7 col-sm-7 col-md-7'>
						<b>Payment Due (Deposit):</b>
					</div>
					<div class='col-xs-5 col-sm-5 col-md-5'>
						<b>$<?php echo $session['due']; ?></b>
					</div>
				</div>
			</div>
			<div class="row book-clear-session">
				<a href='?reset=1' class="btn">Clear Cart</a>
			</div>
		</div>
		<div class='col-xs-12 col-sm-9 col-sm-pull-3 col-md-9 col-md-pull-3'>
			<?php
				if (!empty($session['item'])) {
					foreach($session['item'] as $key => $value) {
						echo '<div class="book-details listings-main row">';
							echo '<div class="listings-name row">';
								echo $value['name'];
							echo '</div>';
							echo '<div class="row">';
								echo $value['rate']['summary'];
							echo '</div>';
							echo '<div class="row">';
								echo $value['date']['summary'];
							echo '</div>';
						echo '</div>';
					}
				}
			?>
			<?php
				echo form_open('booking_validation', array('class'=>'form-horizontal bookings-form'));
				if (validation_errors()) {
					echo '<span class="book-error">';
					echo validation_errors();
					echo '</span>';
				}
			?>
				<div class='book-guest row'>
					<div class='row'>
						<h3 class='heading-subtext'>Enter Guest Information</h3>
					</div>
					<div class="form-group">
						<label for="customer_name" class="col-sm-3 control-label">Full Name <span class="required">*</span></label>
						<div class="col-sm-6">
							<input name="customer_name" type="text" class="form-control must-have" id="customer_name" placeholder="Full Name" value="<?php echo $this->input->post('customer_name'); ?>" data-toggle="tooltip" data-placement="right" title="please enter name"/>
						</div>
						<!--label class="validation col-sm-3 control-label">
							<i class='fa fa-check'></i>
							<i class='fa fa-close'></i>
						</label-->
					</div>
					<div class="form-group">
						<label for="customer_email" class="col-sm-3 control-label">Email <span class="required">*</span></label>
						<div class="col-sm-6">
							<input name="customer_email" type="email" class="form-control must-have" id="customer_email" placeholder="Email" value="<?php echo $this->input->post('customer_email'); ?>" data-toggle="tooltip" data-placement="right" title="please enter email"/>
						</div>
					</div>
					<div class="form-group">
						<label for="customer_phone" class="col-sm-3 control-label">Phone number <span class="required">*</span></label>
						<div class="col-sm-6">
							<input name="customer_phone" type="tel" class="form-control must-have" id="customer_phone" placeholder="phone" value="<?php echo $this->input->post('customer_phone'); ?>" data-toggle="tooltip" data-placement="right" title="please enter phone number"/>
						</div>
					</div>
					<div class="form-group">
						<label for="cust_comp_name" class="col-sm-3 control-label">Company <span class="required">*</span></label>
						<div class="col-sm-6">
							<input name="cust_comp_name" type="text" class="form-control must-have" id="cust_comp_name" placeholder="company" value="<?php echo $this->input->post('cust_comp_name'); ?>" data-toggle="tooltip" data-placement="right" title="please enter company name"/>
						</div>
					</div>
					<div class="form-group">
						<label for="cust_fb" class="col-sm-3 control-label">Facebook Id <span class="required">*</span></label>
						<div class="col-sm-6">
							<input name="cust_fb" type="text" class="form-control must-have" id="cust_fb" placeholder="facebook id" value="<?php echo $this->input->post('cust_fb'); ?>" data-toggle="tooltip" data-placement="right" title="please enter Facebook ID"/>
						</div>
					</div>
					<div class="form-group">
						<label for="customer_address" class="col-sm-3 control-label">Address <span class="required">*</span></label>
						<div class="col-sm-6">
							<input name="customer_address" type="text" class="form-control must-have" id="customer_address" placeholder="address" value="<?php echo $this->input->post('customer_address'); ?>" data-toggle="tooltip" data-placement="right" title="please enter address"/>
						</div>
					</div>
					<div class="form-group">
						<label for="customer_city" class="col-sm-3 control-label">City <span class="required">*</span></label>
						<div class="col-sm-6">
							<input name="customer_city" type="text" class="form-control must-have" id="customer_city" placeholder="city" value="<?php echo $this->input->post('customer_city'); ?>" data-toggle="tooltip" data-placement="right" title="please enter city"/>
						</div>
					</div>
					<div class="form-group">
						<label for="customer_country" class="col-sm-3 control-label">Country <span class="required">*</span></label>
						<div class="col-sm-6">
							<select name="customer_country" id="customer_country" class="form-control must-have bfh-countries" data-country="US"></select>
						</div>
					</div>
					<div class="form-group">
						<label for="customer_region" class="col-sm-3 control-label">State/Region <span class="required">*</span></label>
						<div class="col-sm-6">
							<select name="customer_region" class="form-control must-have bfh-states" id="customer_region" data-country="customer_country" data-toggle="tooltip" data-placement="right" title="please enter state"></select>
						</div>
					</div>
					<div class="form-group">
						<label for="customer_postal_zip" class="col-sm-3 control-label">Postal Code <span class="required">*</span></label>
						<div class="col-sm-6">
							<input name="customer_postal_zip" type="text" class="form-control must-have" id="customer_postal_zip" placeholder="zip" value="<?php echo $this->input->post('customer_postal_zip'); ?>" data-toggle="tooltip" data-placement="right" title="please enter zip code"/>
						</div>
					</div>
					<div class="form-group">
						<label for="note" class="col-sm-3 control-label">Note</label>
						<div class="col-sm-6">
							<textarea name="note" type="text" class="form-control" id="note" value="<?php echo $this->input->post('note'); ?>"></textarea>
						</div>
					</div>
					<div class="form-group">
						<label for="customer_tos_agree" class="col-sm-3 control-label">Terms & Conditions</label>
						<div class="col-sm-9">
							<div class='row terms-conditions'>
	<p>You acknowledge that you have read and have fully understood the terms and conditions of the Customer Contract form and Guest Agreement detailed in <a href='customeragreement'>https://ltstay.com/ltstay/customeragreement</a> governing this booking and you are hereby agreeing to fully comply by it in letter and spirit to the fullest extent.
	You acknowledge that each member of your group will sign the Customer Contract form at the time of your arrival or when asked.</p>

	<p><b>General Cancellation Policies</b><br />If you must cancel your stay and we receive your notice: more than 30 days prior to check in date: full refund minus deposit; 15-29 days prior to check in date: 50% refund; 14 days or less prior to check-in date: no refund</p>

	<p><b>Credit Car Processing Fees</b><br /> We love to keep our costs low and prefer Check payments and ACH transfers. However, if you wish to make full payments through card, we will add 3.5% to such payments as card processing fees.</p>
							</div>
							<div>
								<p></p>
								<input type="checkbox" id="customer_tos_agree" name="customer_tos_agree" value="1" data-toggle="tooltip" data-placement="right" title="please read and agree to Terms of Service."/>
								<b> I have read and agreed to the Terms of Service.</b>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-xs-12 col-sm-12 col-md-3">
					</div>
					<div class="col-xs-12 col-sm-12 col-md-3">
						<button type="submit" class="btn btn-success btn-book">Continue to Payment</button>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-3">
						<a href='?reset=1' class="btn">Cancel</a>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
