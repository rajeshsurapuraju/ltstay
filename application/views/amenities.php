<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class='row amenities page'>
	<div class='heading-wrapper row'>
		<h2 class='heading'> Amenities </h2>
		<div class='heading-subtext'>Just walk in with your bag. We got everything covered!!!</div>
	</div>
	<div class='content-wrapper row'>
		<div class='row'>
			<div class='col-xs-12 col-sm-8 col-md-8'>
				<img src='<?php echo asset_url();?>img/ltstay_room.jpg' class="amenities-img">
			</div>
			<div class='col-xs-12 col-sm-4 col-md-4'>
				<ul class='reset'>
					<h3 class='heading'> Rooms </h3>
					<div class='heading-subtext'>Work or relax. The choice, and the space, is yours.</div>
					<li>
						<i class="fa fa-circle-o" aria-hidden="true"></i>
						<span>Full Furnished Guesthouses with Single, Shared, Family rooms to suit your needs and budget</span>
					</li>
					<li>
						<i class="fa fa-circle-o" aria-hidden="true"></i>
						<span>Ambient lighting and ventilation to keep you happy & healthy</span>
					</li>
					<li>
						<i class="fa fa-circle-o" aria-hidden="true"></i>
						<span>Flexible Workspaces</span>
					</li>
					<li>
						<i class="fa fa-circle-o" aria-hidden="true"></i>
						<span>Table, executive chair, Bedding, Bed , Closets in rooms</span>
					</li>
					<li>
						<i class="fa fa-circle-o" aria-hidden="true"></i>
						<span>Free High Speed 100 Mbps WI-Fi</span>
					</li>
				</ul>
			</div>
		</div>
		<div class='row'>
			<div class='col-xs-12 col-sm-8 col-sm-push-4 col-md-push-4 col-md-8'>
				<img src='<?php echo asset_url();?>img/ltstay_common_area.jpg' class="amenities-img">
			</div>
			<div class='col-xs-12 col-sm-4 col-sm-pull-8 col-md-pull-8 col-md-4'>
				<h3 class='heading'> Common Area </h3>
				<div class='heading-subtext'>The space that works for meetings, meals, or me-time.</div>
				<ul class='reset'>
					<li>
						<i class="fa fa-circle-o" aria-hidden="true"></i>
						<span>TV, Sofa, Dinning table etc in common area</span>
					</li>
					<li>
						<i class="fa fa-circle-o" aria-hidden="true"></i>
						<span>Free High Speed 100 Mbps WI-Fi, Indian Channels, Roku, Netflix</span>
					</li>
					<li>
						<i class="fa fa-circle-o" aria-hidden="true"></i>
						<span>Washer, dryer, iron box, kitchen appliances and everything to make you look perfect</span>
					</li>
					<li>
						<i class="fa fa-circle-o" aria-hidden="true"></i>
						<span>Professional Weekly Cleaning, onsite service manager</span>
					</li>
					<li>
						<i class="fa fa-circle-o" aria-hidden="true"></i>
						<span>General Consumables right from shaving gel, toothbrush, toiletries, laundry detergent, chemicals, tissues, room fresheners etc;</span>
					</li>
				</ul>
			</div>
		</div>
		<div class='row'>
			<div class='col-xs-12 col-sm-8 col-md-8'>
				<img src='<?php echo asset_url();?>img/ltstay_kitchen.jpg' class="amenities-img">
			</div>
			<div class='col-xs-12 col-sm-4 col-md-4'>
				<h3 class='heading'>Kitchen</h3>
				<div class='heading-subtext'>Grab 'n go snacks. Sit-down meals. We've got tastes to suit your speed</div>
				<ul class='reset'>
					<li>
						<i class="fa fa-circle-o" aria-hidden="true"></i>
						<span>Complementary Breakfast items inclusive of almost everything you might need as fruit, juice, milk, cereals, bread, egg, butter, jam, spreads etc;</span>
					</li>
					<li>
						<i class="fa fa-circle-o" aria-hidden="true"></i>
						<span>Cook yourselves or Avail our partner food services</span>
					</li>
					<li>
						<i class="fa fa-circle-o" aria-hidden="true"></i>
						<span>We know you like that early morning coffee or that late night snack. You wont stay hungry as there is something healthy to eat all the time</span>
					</li>
				</ul>
			</div>
		</div>
		<div class='row'>
			<div class='col-xs-12 col-sm-8 col-sm-push-4 col-md-push-4 col-md-8'>
				<img src='<?php echo asset_url();?>img/ltstay_outdoor.jpg' class="amenities-img">
			</div>
			<div class='col-xs-12 col-sm-4 col-sm-pull-8 col-md-pull-8 col-md-4'>
				<h3 class='heading'> OutDoors </h3>
				<div class='heading-subtext'>Rain or Shine!!!!!!!</div>
				<ul class='reset'>
					<li>
						<i class="fa fa-circle-o" aria-hidden="true"></i>
						<span>Most of the Guesthouses have backyards. Stretch your arms, legs, body & mind</span>
					</li>
					<li>
						<i class="fa fa-circle-o" aria-hidden="true"></i>
						<span>Sit down in open spaces for a chat with house mate or phone to one you love</span>
					</li>
					<li>
						<i class="fa fa-circle-o" aria-hidden="true"></i>
						<span>Enjoy the beautiful California weather all the year round</span>
					</li>
				</ul>
			</div>
		</div>
		<div class='row'>
			<div class='col-xs-12 col-sm-8 col-md-8'>
				<img src='<?php echo asset_url();?>img/ltstay_policies.jpg' class="amenities-img">
			</div>
			<div class='col-xs-12 col-sm-4 col-md-4'>
				<h3 class='heading'> Policies </h3>
				<div class='heading-subtext'>We need some policies to solve world peace.</div>
				<ul class='reset'>
					<li>
						<i class="fa fa-circle-o" aria-hidden="true"></i>
						<span>No Smoking in the room. cough! cough!</span>
					</li>
					<li>
						<i class="fa fa-circle-o" aria-hidden="true"></i>
						<span>Pets are not allowed</span>
					</li>
					<li>
						<i class="fa fa-circle-o" aria-hidden="true"></i>
						<span>No fighting over Shahrukh or Salman, Sachin or Dravid, Game of thrones or Breaking Bad. JK!</span>
					</li>
					<li>
						<i class="fa fa-circle-o" aria-hidden="true"></i>
						<span>Cancellation and prepayment policies vary according to room type. Please email to us to learn more about it</span>
					</li>
					<li>
						<i class="fa fa-circle-o" aria-hidden="true"></i>
						<span>When booking more than 5 rooms, different policies and additional supplements may apply</span>
					</li>
				</ul>
			</div>
		</div>
		<div class='row'>
			<div class='col-xs-12 col-sm-8 col-sm-push-4 col-md-push-4 col-md-8'>
				<img src='<?php echo asset_url();?>img/ltstay_partner.jpg' class="amenities-img">
			</div>
			<div class='col-xs-12 col-sm-4 col-sm-pull-8 col-md-pull-8 col-md-4'>
				<h3 class='heading'>Partner Services</h3>
				<div class='heading-subtext'>We got you covered!!!</div>
				<ul class='reset'>
					<li>
						<i class="fa fa-circle-o" aria-hidden="true"></i>
						<span>Need a pickup or drop to airport? Need to go for shopping? Need to meet a friend or relative? Or Need to drop and pick at office daily? Avail our trusted super awesome partner services at reasonable cost</span>
					</li>
					<li>
						<i class="fa fa-circle-o" aria-hidden="true"></i>
						<span>Lazy to cook? Lived life like a king till now? Married? Avail our partner food services delivered two times a day to your home or office on time. Dont stay hungry, Dont stay foolish!</span>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>
