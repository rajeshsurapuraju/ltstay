<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class='search-form'>
	<?php
	$formAttributes = array('class' => 'form-inline', 'role' => 'form', 'id' => 'form-search', 'method' => 'get');
	$inputAttributes = array('class' => 'form-control', 'placeHolder' => 'ex: Sunnyvale');
	$sdinputAttributes = array('class' => 'form-control start_date', 'id' => 'start-date', 'placeHolder' => 'mm/dd/yyyy');
	$edinputAttributes = array('class' => 'form-control end_date', 'id' => 'end-date', 'placeHolder' => 'mm/dd/yyyy');
	$btnAttributes = array('class' => 'btn btn-primary search-btn');

	$start_date = $this->input->get('start_date');
	$end_date = $this->input->get('end_date');
	$location = $this->input->get('location');

	if (!empty($_SESSION['inputs'])) {
		$start_date = $_SESSION['inputs']['start_date'];
		$end_date = $_SESSION['inputs']['end_date'];
		$location = $_SESSION['inputs']['location'];
	}

	echo form_open('search_validation', $formAttributes);
		echo '<div class="form-group">';
			echo '<label class="search-label" for="inputStartDate">Start Date <span class="required">*</span></label>';
			echo '<div class="col-sm-3">';
				echo form_input('start_date', $start_date, $sdinputAttributes);
			echo '</div>';
		echo '</div>';
		echo '<div class="form-group">';
			echo '<label class="search-label" for="inputEndDate">End Date <span class="required">*</span></label>';
			echo '<div class="col-sm-3">';
				echo form_input('end_date', $end_date, $edinputAttributes);
			echo '</div>';
		echo '</div>';
		echo '<div class="form-group">';
			echo '<label class="search-label" for="inputLocation">Location</label>';
			echo '<div class="col-sm-3">';
				echo form_input('location', $location, $inputAttributes);
			echo '</div>';
		echo '</div>';
		echo '<div class="form-group">';
			echo '<div class="col-sm-3">';
				echo form_submit('search', 'Search', $btnAttributes);
			echo '</div>';
		echo '</div>';
	echo form_close();
        if (validation_errors()) {
            echo '<span class="error">';
				echo validation_errors();
            echo '</span>';
        }
	?>
</div>
