<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class='gallery section'>
	<div class='heading-wrapper row'>
		<h2 class='heading'>Gallery</h2>
		<div class='heading-subtext'>View some of on-location pics to know about the services and housing we provide</div>
	</div>
	<div class='content-wrapper row gallery-img-wrapper'>
		<div class='row'>
			<div class="img-wrapper col-xs-12 col-sm-12 col-md-8">
				<a href='<?php echo asset_url();?>img/ltstay_gallery_big.jpg' data-toggle="lightbox" data-gallery="multiimages">
					<img src='<?php echo asset_url();?>img/ltstay_gallery_big.jpg' class='img-gallery'></img>
				</a>
			</div>
			<div class="img-wrapper col-xs-12 col-md-4">
				<div class='row vertical img-wrapper'>
					<a href='<?php echo asset_url();?>img/ltstay_gallery_vertical1.jpg' data-toggle="lightbox" data-gallery="multiimages">
						<img src='<?php echo asset_url();?>img/ltstay_gallery_vertical1.jpg' class='img-gallery'></img>
					</a>
				</div>
				<div class='row vertical img-wrapper'>
					<a href='<?php echo asset_url();?>img/ltstay_gallery_vertical2.jpg' data-toggle="lightbox" data-gallery="multiimages">
						<img src='<?php echo asset_url();?>img/ltstay_gallery_vertical2.jpg' class='img-gallery'></img>
					</a>
				</div>
			</div>
		</div>
		<?php
			$asset_url = asset_url();
			$k=1;
			$hidden = '';
			for($i=1;$i<8;$i++) {
				echo '<div class="row">';
				for($j=1;$j<4;$j++) {
					$imgurl = $asset_url.'img/ltstay_inner'.$k.'.jpg';
					if ($i > 1) {
						$hidden = 'hidden';
					}
					echo "<div class='img-wrapper col-xs-12 col-sm-4 {$hidden}'>";
						echo '<a href="'.$imgurl.'" data-toggle="lightbox" data-gallery="multiimages">';
							echo '<img src="'.$imgurl.'" class="img-gallery img-gallery-row"></img>';
						echo '</a>';
					echo '</div>';
					$k++;
				}
				echo '</div>';
			}
		?>
	</div>
	<div class='btn-wrapper'>
		<a href='<?php echo asset_url();?>img/ltstay_gallery_big.jpg' data-toggle="lightbox" data-gallery="multiimages" class='btn btn-primary btn-lg'>View More Photos →</a>
	</div>
</div>
