<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<?php
if(!empty($start_date) && !empty($end_date)) {
	echo '<div class="search-query row">';
		echo "<div>You searched for</div>";
		echo "<div>Start Date: <b>{$start_date}</b>, End Date: <b>{$end_date}</b>, Location: <b>{$location}</b></div>";
	echo '</div>';
}
?>
