<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>Welcome to Long Term Stay</title>
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<link rel="stylesheet" type="text/css" href="<?php echo asset_url();?>bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="<?php echo asset_url();?>lightbox/css/ekko-lightbox.min.css">
		<link rel="stylesheet" type="text/css" href="<?php echo asset_url();?>font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" type="text/css" href="<?php echo asset_url();?>/css/ltstay.css">
		<link rel="stylesheet" type="text/css" href="<?php echo asset_url();?>jquery-ui/jquery-ui.min.css">
	</head>
	<body class='home'>
		<?php echo $header; ?>
		<div class='main'>
			<div class='booking-carousel'>
				<?php if(isset($body)) echo $body; ?>
				<?php if(isset($hero)) echo $hero; ?>
			</div>
			<?php if(isset($amenities_snippet)) echo $amenities_snippet; ?>
			<?php if(isset($locations_snippet)) echo $locations_snippet; ?>
			<?php if(isset($gallery)) echo $gallery; ?>
		</div>
		<?php echo $footer; ?>
	</body>
	<script src='<?php echo asset_url();?>js/jquery.min.js'></script>
	<script src='<?php echo asset_url();?>jquery-ui/jquery-ui.min.js'></script>
	<script src='<?php echo asset_url();?>bootstrap/js/bootstrap.min.js'></script>
	<script src='<?php echo asset_url();?>lightbox/js/ekko-lightbox.min.js'></script>
	<script src='<?php echo asset_url();?>js/ltstay.js'></script>
	<script>
	$(document).delegate('*[data-toggle="lightbox"]', 'click', function(event) {
		event.preventDefault();
		$(this).ekkoLightbox({
			always_show_close: false,
			keyboard: true
		});
	});
	</script>
</html>
