<div class='footer row'>
	<div class='row'>
		<ul class='footer-about'>
			<li class='footer-about-item'><a href='<?php echo base_url().'about' ?>'>About</a></li>
			<li class='footer-about-item'><a href='<?php echo base_url().'help' ?>'>Help</a></li>
			<li class='footer-about-item'><a href='<?php echo base_url().'help' ?>'>Terms of Use</a></li>
			<li class='footer-about-item'><a href='<?php echo base_url().'help' ?>'>Privacy</a></li>
		</ul>
	</div>
	<div class='row footer-social'>
		<span>
			<a href='https://www.facebook.com/longtermstay/'>
				<i class="fa fa-facebook" aria-hidden="true"></i>
			</a>
		</span>
		<span>
			<a href='https://plus.google.com/+Ltstay/about'>
				<i class="fa fa-twitter" aria-hidden="true"></i>
			</a>
		</span>
		<span>
			<a href='https://twitter.com/uscorppg'>
				<i class="fa fa-google-plus" aria-hidden="true"></i>
			</a>
		</span>
	</div>
	<div class='row'>
			<ul>
				<li><b>Email: </b><a href='mailto:crm@ltstay.com'>crm@ltstay.com</a></li>
				<li><b>Phone: </b>1-844-4LTSTAY</li>
				<li><b>Address: </b>1584 Branham Ln, 65, San Jose 95118</li>
			</ul>
	</div>
	<div class='row'>
		Copyright © 2016 Long Term Stay Inc. All Rights Reserved.
	</div>
</div>
