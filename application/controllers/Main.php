<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Long Term Stay
 *
 * A hotel reservation application
 *
 * @package		Long Term Stay
 * @author		rajeshs142@gmail.com
 */

// ------------------------------------------------------------------------

/**
 * Long Term Stay Main Controller
 *
 * @package		Long Term Stay
 * @subpackage	Controllers
 * @category	Controllers
 * @author		rajeshs142@gmail.com
 * @link		http://www.xcelgaming.com
 */
class Main extends CI_Controller {

	/**
	 * Index
	 *
	 * Home Page
	 *
	 * @access	public
	 * @return	void
	 */
	public function index()
	{
		// Load Home page view
		$this->home();
	}

	// --------------------------------------------------------------------

	/**
	 * Home
	 *
	 * Display Home Page
	 * Page contains Header, Hero, Search, Amenities snippet, Location snippet, Gallery & Footer
	 *
	 * @access	public
	 * @return	void
	 */
	public function home()
	{
		// Retrieve default data
		$data = $this->getDefaultData();

		// Retrieve hero data
		$data['hero'] = $this->getView('hero', null, TRUE);

		// Retrieve amenities snippet data
		$data['amenities_snippet'] = $this->getView('amenities_snippet', null, TRUE);

		// Retrieve locations snippet data
		$data['locations_snippet'] = $this->getView('locations_snippet', null, TRUE);

		// Retrieve gallery data
		$data['gallery'] = $this->getView('gallery', null, TRUE);

		// Load Home page view using Home template
		$this->template->load('home', 'search', $data);
	}

	// --------------------------------------------------------------------

	/**
	 * Locations
	 *
	 * Display Locations Page
	 * Page contains Header, Search, Locations & Footer
	 *
	 * @access	public
	 * @return	void
	 */
	public function locations()
	{
		// Retrieve default data
		$data = $this->getDefaultData();

		// Retrieve search data
		$data['search'] = $this->getView('search', null, TRUE);

		// Load Locations page view using Default template
		$this->template->load('default', 'locations', $data);
	}

	// --------------------------------------------------------------------

	/**
	 * Amenities
	 *
	 * Display Amenities Page
	 * Page contains Header, Search, Amenities & Footer
	 *
	 * @access	public
	 * @return	void
	 */
	public function amenities()
	{
		// Retrieve default data
		$data = $this->getDefaultData();

		// Retrieve search data
		$data['search'] = $this->getView('search', null, TRUE);

		// Load Amenities page view using Default template
		$this->template->load('default', 'amenities', $data);
	}

	// --------------------------------------------------------------------

	/**
	 * Contact
	 *
	 * Display Contact Page
	 * Page contains Header, Contact & Footer
	 *
	 * @access	public
	 * @return	void
	 */
	public function contact()
	{
		// Check if user submitted contact form
		if ($_POST) {
			// Set form validation rules
			$this->form_validation->set_rules('name', '<b>Name</b>', 'required|trim');
			$this->form_validation->set_rules('email', '<b>Email</b>', 'required|trim');
			$this->form_validation->set_rules('message', '<b>Message</b>', 'required|trim');

			// Form validation passed, so continue
			if ($this->form_validation->run()) {
				// Load email library
				$this->load->library('email');

				// Assign to email address
				// TODO: $to should come from config
				$to = 'crm@ltstay.com';

				// Assign email address
				$from = $_POST['email'];

				// Assign name
				$name = $_POST['name'];

				// Assign subject
				// TODO: $subject should come from config
				$subject = 'ltstay email';

				// Assign message
				$message = $_POST['message'];
				// TODO: use a messaging template
				$message = $name.'('.$from.')'.'has sent a message \n'.$message;

				// Setup email library values
				$this->email->from($from, $name);
				$this->email->to($to);
				$this->email->subject($subject);
				$this->email->message($message);

				// Send mail
				$this->email->send();
			}
		}

		// Retrieve deafult data
		$data = $this->getDefaultData();

		// Load Contact page view using Default template
		$this->template->load('default', 'contact', $data);
	}

	// --------------------------------------------------------------------

	/**
	 * Customer Agreement
	 *
	 * Display Customer Agreement Page
	 * Page contains Header, Customer Agreement & Footer
	 *
	 * @access	public
	 * @return	void
	 */
	public function customeragreement()
	{
		// Retrieve deafult data
		$data = $this->getDefaultData();

		// Load Customer Agreement page view using Default template
		$this->template->load('default', 'customeragreement', $data);
	}

	// --------------------------------------------------------------------

	/**
	 * Terms of Use
	 *
	 * Display Terms of Use Page
	 * Page contains Header, Terms of Use & Footer
	 *
	 * @access	public
	 * @return	void
	 */
	public function terms()
	{
		// Retrieve deafult data
		$data = $this->getDefaultData();

		// Load Terms of Use page view using Default template
		$this->template->load('default', 'terms', $data);
	}

	// --------------------------------------------------------------------

	/**
	 * About
	 *
	 * Display About Page
	 * Page contains Header, About & Footer
	 *
	 * @access	public
	 * @return	void
	 */
	public function about()
	{
		// Retrieve deafult data
		$data = $this->getDefaultData();

		// Load About page view using Default template
		$this->template->load('default', 'about', $data);
	}


	// --------------------------------------------------------------------

	/**
	 * Help
	 *
	 * Display Help Page
	 * Page contains Header, Help & Footer
	 *
	 * @access	public
	 * @return	void
	 */
	public function help()
	{
		// Retrieve deafult data
		$data = $this->getDefaultData();

		// Load Help page view using Default template
		$this->template->load('default', 'help', $data);
	}


	// --------------------------------------------------------------------

	/**
	 * Privacy
	 *
	 * Display Privacy Page
	 * Page contains Header, Privacy & Footer
	 *
	 * @access	public
	 * @return	void
	 */
	public function privacy()
	{
		// Retrieve deafult data
		$data = $this->getDefaultData();

		// Load Privacy page view using Default template
		$this->template->load('default', 'privacy', $data);
	}

	// --------------------------------------------------------------------

	/**
	 * Booking Validation
	 *
	 * Does Booking validation and redirects to Payment gateway on success
	 *
	 * @access	public
	 * @return	void
	 */
	public function booking_validation()
	{
		// Set form validation rules
		$this->form_validation->set_rules('customer_name', '<b>Full Name</b>', 'required|trim');
		$this->form_validation->set_rules('customer_email', '<b>Email</b>', 'required|trim');
		$this->form_validation->set_rules('customer_phone', '<b>Phone</b>', 'required|trim');
		$this->form_validation->set_rules('cust_comp_name', '<b>Company Name</b>', 'required|trim');
		$this->form_validation->set_rules('cust_fb', '<b>Facebook id</b>', 'required|trim');
		$this->form_validation->set_rules('customer_address', '<b>Address</b>', 'required|trim');
		$this->form_validation->set_rules('customer_city', '<b>City</b>', 'required|trim');
		$this->form_validation->set_rules('customer_country', '<b>Country</b>', 'required|trim');
		$this->form_validation->set_rules('customer_region', '<b>State</b>', 'required|trim');
		$this->form_validation->set_rules('customer_postal_zip', '<b>Zip</b>', 'required|trim');
		$this->form_validation->set_rules('customer_tos_agree', '<b>Terms of Service</b>', 'required|trim');

		// Form validation passed, so continue
		if ($this->form_validation->run()) {
			// Load booking library
			$this->load->library('Booking');

			// Create new booking object
			$booking = new $this->booking;

			// Assign response from API
			$response = $booking->create($_POST);

			// If response is success
			if($response['request']['status'] == 'OK') {
				// Successful transactions will return a url to be redirected to for payment or an invoice.
				header("Location: {$response['request']['data']['url']}");
				exit;
			} else {
				// Show error on booking failure
				echo($response['request']['error']['details']);
				echo($response['request']['status']);
			}
		} else { // Form validation failed, display errors on booking page
			$this->book();
		}
	}

	// --------------------------------------------------------------------

	/**
	 * Select
	 *
	 * Add to cart if user selects a listing from listings page
	 *
	 * @access	public
	 * @return	void
	 */
	public function select()
	{
		// If user search input data is not empty
		if(!empty($_SESSION['inputs'])) {
			// Assign user search input data
			$data = $_SESSION['inputs'];
		}

		// Load Booking Library
		$this->load->library('Booking');

		// Create new booking object
		$booking = new $this->booking;

		// If user clicks on select button in listings page
	    if(isset($_POST['slip'])) {
			// Add to cart
			$booking->set($_POST['slip']);

			// Add user search input data to session
			$this->session->set_userdata('inputs', $data);

			// Redirect user to booking page
			redirect('book');
		}
	}

	// --------------------------------------------------------------------

	/**
	 * Book
	 *
	 * Display Booking Page
	 * Page contains Header, Booking details & Footer
	 *
	 * @access	public
	 * @return	void
	 */
	public function book()
	{
        // Load Booking Library
		$this->load->library('Booking');

        // Create new booking object
		$booking = new $this->booking;

		// Clear cart if booking is cancelled or user clears cart or session times out
		if(isset($_GET['reset']) || empty($_SESSION['item'])) {
			// Clear booking
			$booking->clear();

			// When booking is cancelled and
			// If user search input data is valid, then go to listings page
			// else redirect to home
			if (!empty($_SESSION['inputs'])) {
				// Assign start_date, end_date, location
				$start_date = $_SESSION['inputs']['start_date'];
				$end_date = $_SESSION['inputs']['end_date'];
				$location = $_SESSION['inputs']['location'];

				// Redirect user to listings page thru search validation
				redirect('search_validation?start_date='.$start_date.'&end_date='.$end_date.'&location='.$location);
			} else {
				// Redirect to home if user search input data is not present
				redirect('home');
			}
			exit;
		}

		// Retrieve deafult data
		$data = $this->getDefaultData();

		// Assign SESSION data
		$data['session'] = $_SESSION;

		// Load Booking Details page view using Default template
		$this->template->load('default', 'book', $data);
	}

	// --------------------------------------------------------------------

	/**
	 * Listings
	 *
	 * Display Listings Page
	 * Page contains Header, Search, Listings & Footer
	 *
	 * @access	public
	 * @return	void
	 */
	public function listings()
	{
		// Load Booking Library
		$this->load->library('Booking');

        // Retrieve deafult data
		$data = $this->getDefaultData();

		// Create variables
		$listings = array();
		$inputs = array();

		// Assign search user input data from search validation
		$userdata = $this->session->all_userdata();

		// Check user search input data is present.
		if(!empty($userdata['inputs'])) {
			// Assign
			$inputs = $userdata['inputs'];
		} else if (!empty($_SESSION['inputs'])) { // Check if session has user search input data
			// Assign
			$inputs = $_SESSION['inputs'];
		}

		// If user search input data is not empty
		if(!empty($inputs)) {
			// Assign
			$start_date = $inputs['start_date'];
			$end_date = $inputs['end_date'];
			$location = $inputs['location'];

			// Query inventory to booking api for listings
			$items = $this->booking->query_inventory(
				array(
					'start_date'=>$start_date,
					'end_date'=>$end_date,
					'keyword' =>$location,
					// change these booking parameters to suit your setup:
					'param'=>array( 'guests' => 1 )
				)
			);

			// Query Categories from booking api for categories
			$categories = $this->booking->query_category();

			// If API returns listings
			if(count($items)) {
				// Loop thru the listings returned by API
				foreach($items as $item_id => $item) {
					// Check if listing item contains rate slip
					if (empty($item['rate']['slip'])) continue;

					// Check if listing item is available
					if ($item['rate']['summary']['title'] === 'Unavailable') continue;

					// Create listings object for each item with fields required to display on page.
					$listings[$item_id] = [
						'item_id' => $item['item_id'],
						'name' => $item['name'],
						'summary' => $item['summary'],
						'slip' => $item['rate']['slip'],
						'available' => $item['rate']['available'],
						'date' => $item['rate']['summary']['date'],
						'category_id' => $item['category_id'],
						'category_name' => isset($categories[$item['category_id']]) ? $categories[$item['category_id']]['name'] : '',
						'price' => empty($item['rate']['summary']['price']['title']) ? $item['rate']['summary']['price']['total'] : $item['rate']['summary']['price']['title'],
						'price_unit' => empty($item['rate']['summary']['price']['unit']) ? 'for 30 days' : $item['rate']['summary']['price']['unit'],
						'price_details' => $item['rate']['summary']['details'],
						'image' => $item['image'],
						'location' => isset($item['meta']['location']) ? $item['meta']['location'] : ''
					];
				}
			}

			// Assign
			$data['listings'] = $listings;
			$data['search'] = $this->getView('search', $_SESSION['inputs'], TRUE);
			$data['searchquery'] = $this->getView('searchquery', $_SESSION['inputs'], TRUE);

			// Load Listings page view using Default template
			$this->template->load('default', 'listings', $data);
		} else {
			// Redirect to home if empty user search data
			redirect('home');
		}
	}


	// --------------------------------------------------------------------

	/**
	 * Search Validation
	 *
	 * Does Search Validation and navigates to listings page on success
	 *
	 * @access	public
	 * @return	void
	 */
	public function search_validation()
	{
		// Code Igniter does not do automatic form validation for GET data
		// So we set user input data for forms with method GET
		// remove below line for POST data
		$this->form_validation->set_data($_GET);

        // Set form validation rules
		$this->form_validation->set_rules('location', '<b>Location</b>', 'trim');
		$this->form_validation->set_rules('start_date', '<b>Start Date</b>', 'required|trim');
		$this->form_validation->set_rules('end_date', '<b>End Date</b>', 'required|trim|callback_validate_enddate');

		// Form validation passed, so continue
		if ($this->form_validation->run()) {
			// Assign
			$data = array(
				'start_date'=> date($this->input->get('start_date', TRUE)),
				'end_date'=> date($this->input->get('end_date', TRUE)),
				'location'=> $this->input->get('location', TRUE)
			);

			// Set session user data so that it is used to fill search inputs in other pages
			$this->session->set_userdata('inputs', $data);

			// Redirect to listings as search validation passed
			redirect('listings');
		} else {
			// Redirect to home as search validation failed and display errors
			$this->home();
		}
	}


	// --------------------------------------------------------------------

	/**
	 * Get Default Data
	 *
	 * Getter Function
	 *
	 * @access	public
	 * @return	Array with header & footer
	 */
	public function getDefaultData()
	{
		// Array with Header  & Footer as these are present in all pages
		$data = array('header' => $this->getView('header', null, TRUE), 'footer' => $this->getView('footer', null, TRUE));

		// Return
		return $data;
	}


	// --------------------------------------------------------------------

	/**
	 * Get View
	 *
	 * Getter function. Generic function to load view
	 *
	 * @access	public
	 * @return	Object which contains view
	 */
	public function getView($viewname, $data = null, $flag)
	{
		// Return
		return $this->load->view($viewname, $data, $flag);
	}


	// --------------------------------------------------------------------

	/**
	 * Validate End date
	 *
	 * Call back function for search validation end date rule
	 *
	 * @access	public
	 * @return	Boolean true or false
	 */
	public function validate_enddate($end_date) {
		// Assign
		$start_date = $this->input->get('start_date');

		// Case 1
		// Check end date is less than todays date
		if ($end_date && strtotime($end_date) < strtotime(date('Y-m-d'))) {
			// Set Error message
			$this->form_validation->set_message('validate_enddate', 'The %s field can not be before current date');

			// Return
			return false;
		}

		// Case 2
		// Check end date is less than start date
		if ($end_date && strtotime($end_date) < strtotime($start_date)) {
			// Set Error message
			$this->form_validation->set_message('validate_enddate', 'The %s field can not be before start date');

			// Return
			return false;
		}

		// Case 3
		// Check if end date is less than start date by 30 days
		// Remove if this does not fit your use case
		if ($end_date && (strtotime($end_date) - strtotime($start_date))/86400 < 30) {
			// Set Error message
			$this->form_validation->set_message('validate_enddate', 'The %s field should be 30 days more than start date');

			// Return
			return false;
		}

		// Return
		return true;
	}

}
